package com.example.stephan.cmgtest.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.stephan.cmgtest.R;
import com.example.stephan.cmgtest.model.User;

import java.util.List;

/**
 * Created by Stephan on 23.07.2016.
 */
public class ListUsersAdapter extends UsersBaseAdapter {

    public ListUsersAdapter(List<User> users)
    {
        super(users);
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_user_item, null);
        return new UserViewHolder(layoutView);
    }
}
