package com.example.stephan.cmgtest.views.adapters;

import android.support.v7.widget.RecyclerView;

import com.example.stephan.cmgtest.infrastructure.ImageLoaderConfigProvider;
import com.example.stephan.cmgtest.model.User;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.List;

/**
 * Created by Stephan on 22.07.2016.
 */
public abstract class UsersBaseAdapter extends RecyclerView.Adapter<UserViewHolder> {

    protected List<User> mUsers;
    protected DisplayImageOptions mAvatarOptions;

    public UsersBaseAdapter(List<User> users)
    {
        mUsers = users;
        mAvatarOptions = ImageLoaderConfigProvider.getOptions();
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = mUsers.get(position);
        holder.showUserInfo(user, mAvatarOptions, position);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void updateData(List<User> users)
    {
        mUsers = users;
        notifyDataSetChanged();
    }

    public void clean()
    {
        mUsers.clear();
        notifyDataSetChanged();
    }

}
