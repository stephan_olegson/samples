package com.example.stephan.cmgtest.views.interfaces;

/**
 * Created by Stephan on 23.07.2016.
 */
public interface IUsersViewHolderXmlProvider {
    int getTrackItemXmlPatternId();
}
