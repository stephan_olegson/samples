package com.example.stephan.cmgtest.infrastructure;

import android.os.Handler;

import com.activeandroid.query.Select;
import com.example.stephan.cmgtest.infrastructure.interfaces.IPersistance;
import com.example.stephan.cmgtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.cmgtest.model.User;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;
import rx.functions.Action2;

/**
 * Created by Stephan on 01.08.2016.
 */
public class OfflineRestClient implements IRestClient {

    private IPersistance mPersistane;

    public OfflineRestClient(IPersistance persistance) {
        mPersistane = persistance;
    }

    @Override
    public void getUsers(int last_id, Action2<List<User>, Boolean> onResult, Action1<Throwable> err) {
         List<User> users = new ArrayList<>();
        try
        {
            users = mPersistane.getUsers(last_id);
        }
        catch (Exception ex) {
            err.call(ex);
        }
        onResult.call(users, false);

    }
}
