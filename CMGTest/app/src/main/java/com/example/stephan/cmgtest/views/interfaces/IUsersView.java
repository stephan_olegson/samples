package com.example.stephan.cmgtest.views.interfaces;

import com.example.stephan.cmgtest.model.User;

import java.util.List;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface IUsersView {
    void showNotification(String text);
    void isProgressVivible(boolean isVisible);
    void setUsers(List<User> users);
}
