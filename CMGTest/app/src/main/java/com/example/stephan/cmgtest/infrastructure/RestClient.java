package com.example.stephan.cmgtest.infrastructure;

import com.example.stephan.cmgtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.cmgtest.infrastructure.interfaces.GithubApi;
import com.example.stephan.cmgtest.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.schedulers.Schedulers;

/**
 * Created by Stephan on 21.07.2016.
 */
public class RestClient implements IRestClient {

    private static final String BASE_URL = " https://api.github.com/";
    private GithubApi mService;

    public RestClient() {

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();

        mService = retrofit.create(GithubApi.class);

    }

    @Override
    public void getUsers(int last_id, Action2<List<User>, Boolean> onResult, Action1<Throwable> onError) {
        Observable<List<User>> users = mService.getUsers(last_id);
        users.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ret -> onResult.call(ret, true)
                        , err -> onError.call(err)
                );
    }


}
