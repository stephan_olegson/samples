package com.example.stephan.cmgtest;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.activeandroid.ActiveAndroid;
import com.example.stephan.cmgtest.infrastructure.ImageLoaderConfigProvider;
import com.example.stephan.cmgtest.infrastructure.OfflineRestClient;
import com.example.stephan.cmgtest.infrastructure.RestClient;
import com.example.stephan.cmgtest.infrastructure.SQLPersistance;
import com.example.stephan.cmgtest.infrastructure.interfaces.IPersistance;
import com.example.stephan.cmgtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.cmgtest.model.User;
import com.pixplicity.easyprefs.library.Prefs;
import com.activeandroid.Configuration;

import rx.functions.Func0;

/**
 * Created by Stephan on 21.07.2016.
 */
public class CMGTestApp extends Application {

    private IRestClient mRestClient;
    private IRestClient mOfflineClient;
    private Func0<IRestClient> mRestClientGetter;
    private IPersistance mPersistane;

    @Override
    public void onCreate()
    {
        initSingletons();
        super.onCreate();
    }

    protected void initSingletons()
    {
        initPersistance();
        ImageLoaderConfigProvider.InitializeImageLoader(getApplicationContext());
        mRestClient = new RestClient();
        mOfflineClient = new OfflineRestClient(mPersistane);

        mRestClientGetter = () -> {
            if(isNetworkAvailable())
                return mRestClient;
            else
                return mOfflineClient;
        };
    }

    private void initPersistance() {
        Configuration.Builder configurationBuilder = new Configuration.Builder(this);
        configurationBuilder.addModelClass(User.class);
        configurationBuilder.setDatabaseVersion(1);
        configurationBuilder.setDatabaseName("persistance.db");
        ActiveAndroid.initialize(configurationBuilder.create());

        mPersistane = new SQLPersistance();
    }

    public IPersistance getPersistance()
    {
        return mPersistane;
    }

    public Func0<IRestClient> getRestClient()
    {
        return mRestClientGetter;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
