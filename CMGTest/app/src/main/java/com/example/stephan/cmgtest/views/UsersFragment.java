package com.example.stephan.cmgtest.views;

import android.os.Bundle;
import android.service.voice.VoiceInteractionService;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.stephan.cmgtest.CMGTestApp;
import com.example.stephan.cmgtest.R;
import com.example.stephan.cmgtest.infrastructure.interfaces.IPersistance;
import com.example.stephan.cmgtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.cmgtest.model.User;
import com.example.stephan.cmgtest.presenters.UsersPresenter;
import com.example.stephan.cmgtest.presenters.interfaces.IUsersPresenter;
import com.example.stephan.cmgtest.utils.UserTableItemDecoration;
import com.example.stephan.cmgtest.views.adapters.ListUsersAdapter;
import com.example.stephan.cmgtest.views.adapters.UsersBaseAdapter;
import com.example.stephan.cmgtest.views.interfaces.IUsersView;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func0;

/**
 * Created by Stephan on 21.07.2016.
 */
public class UsersFragment extends Fragment implements IUsersView {

    private IUsersPresenter mPresenter;
    private RecyclerView mUsersListView;
    private UsersBaseAdapter mCurrentAdapter;
    private ProgressBar mLoadingView;

    int mPastVisiblesItems, mVisibleItemCount, mTotalItemCount;

    public static UsersFragment newInstance()
    {
        UsersFragment ret = new UsersFragment();
        ret.setRetainInstance(true);
        return ret;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Func0<IRestClient> restClient = ((CMGTestApp)getActivity().getApplication()).getRestClient();
        IPersistance persistance = ((CMGTestApp)getActivity().getApplication()).getPersistance();
        mPresenter = new UsersPresenter(this, restClient, persistance);
        mCurrentAdapter = new ListUsersAdapter(new ArrayList<>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.user_list_fragment, container, false);

        mUsersListView = (RecyclerView)view.findViewById(R.id.users_list_view);

        mUsersListView.setHasFixedSize(true);
        mUsersListView.setLayoutManager(new LinearLayoutManager(getContext()));
        mUsersListView.addItemDecoration(new UserTableItemDecoration(20,10,10,0));

        mUsersListView.setAdapter(mCurrentAdapter);
        mUsersListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0)
                {
                    mVisibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    mTotalItemCount = recyclerView.getLayoutManager().getItemCount();
                    RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
                    mPastVisiblesItems = ((LinearLayoutManager)manager).findFirstVisibleItemPosition();

                    if ( (mVisibleItemCount + mPastVisiblesItems) >= mTotalItemCount)
                    {
                        mPresenter.getNewUsersChunk();
                    }
                }
            }
        });
        mLoadingView = (ProgressBar)view.findViewById(R.id.loading_view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onReadyToWork();
    }

    @Override
    public void showNotification(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void isProgressVivible(boolean isVisible) {
        mLoadingView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setUsers(List<User> users) {
        mCurrentAdapter.updateData(users);
    }

}
