package com.example.stephan.cmgtest.views.interfaces;

import com.example.stephan.cmgtest.views.adapters.UsersBaseAdapter;

/**
 * Created by Stephan on 23.07.2016.
 */
public interface IUsersAdaptersProvider {
    UsersBaseAdapter getAdapter();
}
