package com.example.stephan.cmgtest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.stephan.cmgtest.views.UsersFragment;

public class MainActivity extends AppCompatActivity {

    private Fragment mAactivityContent;
    private final String TAG = "ACTIVITY_CONTENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TAG);
        if(fragment == null)
        {
            mAactivityContent = UsersFragment.newInstance();
            fragmentManager.beginTransaction()
                    .add(R.id.fragmentHolder, mAactivityContent, TAG)
                    .commit();
        }
        else
        {
            mAactivityContent = fragment;
        }
    }
}
