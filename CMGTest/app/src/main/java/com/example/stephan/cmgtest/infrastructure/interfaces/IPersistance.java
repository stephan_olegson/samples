package com.example.stephan.cmgtest.infrastructure.interfaces;

import com.example.stephan.cmgtest.model.User;

import java.util.List;

/**
 * Created by Stephan on 01.08.2016.
 */
public interface IPersistance {
    void saveUsers(List<User> users);
    List<User> getUsers(int last_id);
}
