package com.example.stephan.cmgtest.presenters;

import android.os.AsyncTask;

import com.example.stephan.cmgtest.infrastructure.interfaces.IPersistance;
import com.example.stephan.cmgtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.cmgtest.model.User;
import com.example.stephan.cmgtest.presenters.interfaces.IUsersPresenter;
import com.example.stephan.cmgtest.views.interfaces.IUsersView;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func0;

/**
 * Created by Stephan on 21.07.2016.
 */
public class UsersPresenter implements IUsersPresenter {

    private final IUsersView mView;
    private Func0<IRestClient> mRestClient;
    private int mLastUserId;
    private List<User> mUsers;
    private IPersistance mPersistance;
    private boolean isLoadingInProgress;

    public UsersPresenter(IUsersView view,
                          Func0<IRestClient> restClient, IPersistance persistance) {
        mView = view;
        mRestClient = restClient;
        mLastUserId = 0;
        mUsers = new ArrayList<>();
        mPersistance = persistance;
    }

    @Override
    public void getNewUsersChunk() {
        if(isLoadingInProgress)
            return;
        isLoadingInProgress = true;
        mView.isProgressVivible(true);
        mRestClient.call().getUsers(mLastUserId, this::onUsersReceived, this::onError);
    }

    @Override
    public void onReadyToWork() {
        mRestClient.call().getUsers(mLastUserId, this::onUsersReceived, this::onError);
    }

    private void onUsersReceived(List<User> users, boolean isOnline) {
        mView.isProgressVivible(false);
        if(users.isEmpty())
        {
            isLoadingInProgress = false;
            return;
        }
        mUsers.addAll(users);

        if(isOnline)
            mPersistance.saveUsers(users);

        mLastUserId = mUsers.get(mUsers.size()-1).getUserId();
        mView.setUsers(mUsers);
        isLoadingInProgress = false;
    }

    private void onError(Throwable throwable) {
        isLoadingInProgress = false;
        mView.isProgressVivible(false);
        mView.isProgressVivible(false);
        mView.showNotification(throwable.getMessage());
    }

}
