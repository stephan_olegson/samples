package com.example.stephan.cmgtest.infrastructure.interfaces;

import com.example.stephan.cmgtest.model.User;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface GithubApi
{
    @GET("users")
    Observable<List<User>> getUsers(@Query("since") int user_id);
}