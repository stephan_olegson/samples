package com.example.stephan.cmgtest.infrastructure;

import android.os.AsyncTask;

import com.activeandroid.query.Select;
import com.example.stephan.cmgtest.infrastructure.interfaces.IPersistance;
import com.example.stephan.cmgtest.model.User;

import java.util.List;

/**
 * Created by Stephan on 01.08.2016.
 */
public class SQLPersistance implements IPersistance {

    @Override
    public void saveUsers(List<User> users) {
        SaveUsersTask task = new SaveUsersTask();
        task.execute(users);
    }

    @Override
    public List<User> getUsers(int last_id) {
        return new Select().from(User.class).where("users.user_id > " + String.valueOf(last_id)).limit(30).execute();
    }

    class SaveUsersTask extends AsyncTask<List<User>, Void, Void>
    {
        @Override
        protected Void doInBackground(List<User>... params) {
            List<User> users = params[0];
            if(users != null)
            {
                for(User user : users)
                {
                    user.save();
                }
            }
            return null;
        }
    }
}
