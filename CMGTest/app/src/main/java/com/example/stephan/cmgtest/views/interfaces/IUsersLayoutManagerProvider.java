package com.example.stephan.cmgtest.views.interfaces;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Stephan on 23.07.2016.
 */
public interface IUsersLayoutManagerProvider {
    RecyclerView.LayoutManager getLayoutManager();

}
