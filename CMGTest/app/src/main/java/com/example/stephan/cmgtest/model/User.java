package com.example.stephan.cmgtest.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Stephan on 01.08.2016.
 */
@Table(name = "users")
public class User extends Model {

    public User() {
        super();
    }

    @Expose
    @Column(name = "user_login", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @SerializedName("login")
    private String mLogin;

    @Expose
    @Column(name = "user_id")
    @SerializedName("id")
    private Integer mId;

    @Expose
    @Column(name = "user_avatar_url")
    @SerializedName("avatar_url")
    private String mAvatarUrl;

    public String getLogin() {
        return mLogin;
    }

    public Integer getUserId() {
        return mId;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }
}
