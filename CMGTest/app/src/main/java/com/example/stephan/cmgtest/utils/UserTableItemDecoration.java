package com.example.stephan.cmgtest.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Stephan on 23.07.2016.
 */

public class UserTableItemDecoration extends RecyclerView.ItemDecoration {
    private int mTop;
    private int mLeft;
    private int mRight;
    private int mBottom;

    public UserTableItemDecoration(int top, int left, int right, int bottom) {
        mTop = top;
        mLeft = left;
        mRight = right;
        mBottom = bottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.left = mLeft;
        outRect.right = mRight;
        outRect.bottom = mBottom;
        outRect.top = mTop;
    }
}