package com.example.stephan.cmgtest.infrastructure.interfaces;

import com.example.stephan.cmgtest.model.User;

import java.util.List;

import rx.functions.Action1;
import rx.functions.Action2;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface IRestClient {
    void getUsers(int last_id, Action2<List<User>, Boolean> onResult, Action1<Throwable> err);
}
