package com.example.stephan.cmgtest.presenters.interfaces;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface IUsersPresenter {
    void getNewUsersChunk();
    void onReadyToWork();
}
