package com.example.stephan.cmgtest.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stephan.cmgtest.R;
import com.example.stephan.cmgtest.model.User;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Stephan on 22.07.2016.
 */
public class UserViewHolder extends RecyclerView.ViewHolder {

    private ImageView mAvatar;
    private TextView mUserName;
    public View container;

    public UserViewHolder(View itemView) {
        super(itemView);
        mAvatar = (ImageView)itemView.findViewById(R.id.user_avatar);
        mUserName = (TextView)itemView.findViewById(R.id.user_login);
        container = itemView.findViewById(R.id.container);
    }

    public void showUserInfo(User user, DisplayImageOptions options, int index)
    {
        showUserName(user.getLogin());
        showAvatar(user.getAvatarUrl(), options);
    }

    private void showUserName(String name)
    {
        mUserName.setText(name);
    }

    private void showAvatar(String url, DisplayImageOptions options)
    {
        ImageLoader.getInstance().displayImage(url, mAvatar, options);
    }
}
