package com.example.stephan.intechtest.presenters.interfaces;

import com.example.stephan.intechtest.model.Track;

import java.util.List;

/**
 * Created by Stephan on 22.07.2016.
 */
public interface IPlayerPresenter {
    void onResume();
    void nextTrackPressed();
    void prevTrackPressed();
    void playTrackPressed();
    void setData(List<Track> tracks, int showTrackIndex);
}
