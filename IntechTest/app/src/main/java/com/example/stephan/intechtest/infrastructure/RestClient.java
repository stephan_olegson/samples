package com.example.stephan.intechtest.infrastructure;

import com.example.stephan.intechtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.intechtest.infrastructure.interfaces.ItunesApi;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.model.TrackList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Stephan on 21.07.2016.
 */
public class RestClient implements IRestClient {

    private static final String BASE_URL = "https://itunes.apple.com/";
    private ItunesApi mService;


    public RestClient() {

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();

        mService = retrofit.create(ItunesApi.class);

    }

    @Override
    public void getTracksByName(String query, Action1<List<Track>> onResult, Action1<Throwable> onError) {

        Observable<TrackList> broadcasts = mService.getTracksByName(query, "song");

        broadcasts.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ret -> onResult.call(ret.getResults())
                        , err -> onError.call(err)
                );
    }
}
