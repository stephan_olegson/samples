package com.example.stephan.intechtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Stephan on 21.07.2016.
 */
public class Track implements Parcelable {

    @SerializedName("artistName")
    @Expose
    private String mArtistName;

    @SerializedName("collectionName")
    @Expose
    private String mAlbumName;

    @SerializedName("trackName")
    @Expose
    private String mTrackName;

    @SerializedName("artworkUrl100")
    @Expose
    private String mCoverUrl;



    public String getArtistName() {
        return mArtistName;
    }

    public String getAlbumName() {
        return mAlbumName;
    }

    public String getTrackName() {
        return mTrackName;
    }

    public String getCoverUrl() {
        return mCoverUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{mArtistName, mAlbumName, mTrackName, mCoverUrl});
    }

    public Track(Parcel in){
        String[] data = new String[4];

        in.readStringArray(data);
        mArtistName = data[0];
        mAlbumName = data[1];
        mTrackName = data[2];
        mCoverUrl = data[3];
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Track createFromParcel(Parcel in) {
            return new Track(in);
        }

        public Track[] newArray(int size) {
            return new Track[size];
        }
    };
}
