package com.example.stephan.intechtest;

import android.app.Application;
import android.content.ContextWrapper;

import com.example.stephan.intechtest.infrastructure.ImageLoaderConfigProvider;
import com.example.stephan.intechtest.infrastructure.RestClient;
import com.example.stephan.intechtest.infrastructure.interfaces.IRestClient;
import com.pixplicity.easyprefs.library.Prefs;

/**
 * Created by Stephan on 21.07.2016.
 */
public class IntechTestApp extends Application {

    private IRestClient mRestClient;

    @Override
    public void onCreate()
    {
        initSingletons();
        super.onCreate();
    }

    protected void initSingletons()
    {
        ImageLoaderConfigProvider.InitializeImageLoader(getApplicationContext());
        mRestClient = new RestClient();

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public IRestClient getRestClient()
    {
        return mRestClient;
    }
}
