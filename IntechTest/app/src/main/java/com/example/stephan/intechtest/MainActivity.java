package com.example.stephan.intechtest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.stephan.intechtest.infrastructure.RestClient;
import com.example.stephan.intechtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.model.TrackList;
import com.example.stephan.intechtest.presenters.interfaces.IFragmentChanger;
import com.example.stephan.intechtest.views.PlayerFragment;
import com.example.stephan.intechtest.views.TracksFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IFragmentChanger {

    IRestClient _client;//remove me

    private Fragment mAactivityContent;
    private final String TAG = "ACTIVITY_CONTENT";
    private final String PLAYER_FRAGMENT_TAG = "PLAYER_FRAGMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TAG);
        if(fragment == null)
        {
            FragmentTransaction tran = fragmentManager.beginTransaction();
            tran.add(PlayerFragment.newInstance(), PLAYER_FRAGMENT_TAG);

            mAactivityContent = TracksFragment.newInstance();
            fragmentManager.beginTransaction()
                    .add(R.id.fragmentHolder, mAactivityContent, TAG)
                    .commit();
        }
        else
        {
            mAactivityContent = fragment;
        }
    }

    private void changeFragment(Fragment frg)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentHolder, frg)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void changeToPlayer(List<Track> tracks, int currentIndex) {
        Fragment frg = getSupportFragmentManager().findFragmentByTag(PLAYER_FRAGMENT_TAG);
        if(frg==null)
            frg = PlayerFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("TRACKS", (ArrayList<Track>)tracks);
        bundle.putInt("TRACK_INDEX", currentIndex);
        frg.setArguments(bundle);
        changeFragment(frg);
    }
}
