package com.example.stephan.intechtest.views.interfaces;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * Created by Stephan on 22.07.2016.
 */
public interface IPlayerView {
    void showCover(String url, DisplayImageOptions options);
    void showTrackName(String name);
    void showArtistName(String name);
    void changePlayIcon(int id);
}
