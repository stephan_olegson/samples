package com.example.stephan.intechtest.views;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.stephan.intechtest.IntechTestApp;
import com.example.stephan.intechtest.MainActivity;
import com.example.stephan.intechtest.R;
import com.example.stephan.intechtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.presenters.TracksPresenter;
import com.example.stephan.intechtest.presenters.interfaces.IFragmentChanger;
import com.example.stephan.intechtest.presenters.interfaces.ITracksPresenter;
import com.example.stephan.intechtest.utils.TrackTableItemDecoration;
import com.example.stephan.intechtest.views.adapters.ListTracksAdapter;
import com.example.stephan.intechtest.views.adapters.TableTracksAdapter;
import com.example.stephan.intechtest.views.adapters.TracksListAppearanceController;
import com.example.stephan.intechtest.views.adapters.TracksBaseAdapter;
import com.example.stephan.intechtest.views.interfaces.ISwitchViewIcontProvider;
import com.example.stephan.intechtest.views.interfaces.ITracksAdaptersProvider;
import com.example.stephan.intechtest.views.interfaces.ITracksLayoutManagerProvider;
import com.example.stephan.intechtest.views.interfaces.ITracksView;
import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Stephan on 21.07.2016.
 */
public class TracksFragment extends Fragment implements ITracksView {

    private ITracksPresenter mPresenter;
    private RecyclerView mTrackList;
    private TracksBaseAdapter mCurrentAdapter;
    private String mCurrentSearchQuery;
    private View mProgressView;
    private MenuItem mSwitchViewMenuItem;

    private ITracksLayoutManagerProvider mLayoutManagerProvider;
    private ITracksAdaptersProvider mTracksAdapterProvider;
    private ISwitchViewIcontProvider mIconProvider;
    private IFragmentChanger mFragmentChanger;

    public static TracksFragment newInstance()
    {
        TracksFragment ret = new TracksFragment();
        ret.setRetainInstance(true);
        return ret;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        IRestClient restClient = ((IntechTestApp)getActivity().getApplication()).getRestClient();
        TracksListAppearanceController tracksAppearanceController = new TracksListAppearanceController(getContext());
        mPresenter = new TracksPresenter(this, restClient, getContext(), tracksAppearanceController);
        tracksAppearanceController.setAdapters(new ListTracksAdapter(new ArrayList<>(),this::onTrackClicked),
                new TableTracksAdapter(new ArrayList<>(), this::onTrackClicked));

        mLayoutManagerProvider = tracksAppearanceController;
        mTracksAdapterProvider = tracksAppearanceController;
        mIconProvider = tracksAppearanceController;

        mCurrentSearchQuery = "";
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFragmentChanger = (MainActivity)getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.track_list_fragment, container, false);

        mTrackList = (RecyclerView)view.findViewById(R.id.track_list_view);

        mTrackList.setHasFixedSize(true);
        mTrackList.setLayoutManager(mLayoutManagerProvider.getLayoutManager());
        mTrackList.addItemDecoration(new TrackTableItemDecoration(20,10,10,0));

        mCurrentAdapter = mTracksAdapterProvider.getAdapter();
        mTrackList.setAdapter(mCurrentAdapter);

        mProgressView = view.findViewById(R.id.progress_bar);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem searchMenuItem = menu.findItem(R.id.search_bar);
        mSwitchViewMenuItem = menu.findItem(R.id.change_view);
        mSwitchViewMenuItem.setIcon(mIconProvider.getMenuIcon());

        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        SearchManager manager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        searchView.setSearchableInfo(manager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQuery(mCurrentSearchQuery, false);

        RxSearchView.queryTextChanges(searchView)
        .filter(charSequence -> !charSequence.toString().equals(mCurrentSearchQuery))
        .debounce(500, TimeUnit.DAYS.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(charSequence ->{
            mCurrentSearchQuery = charSequence.toString();
            mPresenter.setSearchQuery(mCurrentSearchQuery);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.change_view:
                mPresenter.switchTracksView();
                return true;
        }
        return false;
    }

    @Override
    public void showNotification(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void isProgressVivible(boolean isVisible) {
        mProgressView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setChangeViewIcon(int iconId) {
        mSwitchViewMenuItem.setIcon(iconId);
    }

    @Override
    public void changeTracksAppearance() {
        mTrackList.setLayoutManager(mLayoutManagerProvider.getLayoutManager());
        mTrackList.setAdapter(mTracksAdapterProvider.getAdapter());
    }

    @Override
    public void openPlayer(List<Track> tracks, int currentIndex) {
        mFragmentChanger.changeToPlayer(tracks, currentIndex);
    }

    private void onTrackClicked(List<Track> tracks, int index) {
        mPresenter.onTrackClicked(tracks, index);
    }



}
