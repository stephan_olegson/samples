package com.example.stephan.intechtest.views.adapters;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.stephan.intechtest.R;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.views.interfaces.ISwitchViewIcontProvider;
import com.example.stephan.intechtest.views.interfaces.ITrackViewHolderXmlProvider;
import com.example.stephan.intechtest.views.interfaces.ITracksAdaptersProvider;
import com.example.stephan.intechtest.views.interfaces.ITracksLayoutManagerProvider;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

/**
 * Created by Stephan on 23.07.2016.
 */
public class TracksListAppearanceController implements
        ITrackViewHolderXmlProvider,
        ITracksAdaptersProvider,
        ITracksLayoutManagerProvider,
        ISwitchViewIcontProvider{

    private final String APPEARANCE_KEY = "TrackAppearance";
    private ListTracksAdapter mListAdapter;
    private TableTracksAdapter mTableAdapter;

    public enum Appearance
    {
        Table,
        List
    }

    private Appearance mCurrentAppearance;
    private Context mContext;

    public TracksListAppearanceController(Context context) {

        if(!Prefs.contains(APPEARANCE_KEY))
        {
            Prefs.putString(APPEARANCE_KEY,Appearance.Table.name());
        }
        mCurrentAppearance = Appearance.valueOf(Prefs.getString(APPEARANCE_KEY, Appearance.Table.name()));
        mContext = context;
    }

    public void switchAppearance() {
        mCurrentAppearance = mCurrentAppearance == Appearance.Table ? Appearance.List : Appearance.Table;
        Prefs.putString(APPEARANCE_KEY, mCurrentAppearance.name());
    }

    @Override
    public int getMenuIcon() {
        switch (mCurrentAppearance) {
            case Table:
                return R.drawable.track_list_view;
            case List:
                return R.drawable.track_table_view;
            default:
                return R.drawable.track_list_view;
        }
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager()
    {
        int colsCount = 2;
        switch (mCurrentAppearance)
        {
            case Table:
                colsCount = mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT
                        ? 2
                        : 3;
                return new GridLayoutManager(mContext, colsCount);
            case List:
                return new LinearLayoutManager(mContext);
            default:
                return new GridLayoutManager(mContext, colsCount);
        }
    }

    @Override
    public int getTrackItemXmlPatternId()
    {
        switch (mCurrentAppearance) {
            case Table:
                return R.layout.table_track_item;
            case List:
                return R.layout.list_track_item;
            default:
                return R.layout.table_track_item;
        }
    }

    @Override
    public TracksBaseAdapter getAdapter()
    {
        switch (mCurrentAppearance) {
            case Table:
                return mTableAdapter;
            case List:
                return mListAdapter;
            default:
                return mTableAdapter;
        }
    }

    public void setAdapters(ListTracksAdapter listAdapter, TableTracksAdapter tableAdapter)
    {
        mListAdapter = listAdapter;
        mTableAdapter = tableAdapter;
    }

    public void updateAdapters(List<Track> tracks)
    {
        mListAdapter.updateData(tracks);
        mTableAdapter.updateData(tracks);
    }

    public void cleanAdapters()
    {
        mListAdapter.clean();
        mTableAdapter.clean();
    }

}
