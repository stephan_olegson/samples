package com.example.stephan.intechtest.infrastructure.interfaces;

import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.model.TrackList;

import java.util.List;

import rx.functions.Action1;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface IRestClient {
    void getTracksByName(String query, Action1<List<Track>> onResult, Action1<Throwable> err);
}
