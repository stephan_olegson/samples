package com.example.stephan.intechtest.presenters;

import android.content.Context;
import android.widget.Toast;

import com.example.stephan.intechtest.R;
import com.example.stephan.intechtest.infrastructure.interfaces.IRestClient;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.model.TrackList;
import com.example.stephan.intechtest.presenters.interfaces.IFragmentChanger;
import com.example.stephan.intechtest.presenters.interfaces.ITracksPresenter;
import com.example.stephan.intechtest.views.adapters.TracksListAppearanceController;
import com.example.stephan.intechtest.views.interfaces.ITracksView;

import java.util.List;

/**
 * Created by Stephan on 21.07.2016.
 */
public class TracksPresenter implements ITracksPresenter{

    private final ITracksView mView;
    private IRestClient mRestClient;
    private Context mContext;
    private List<Track> mTracks;
    private TracksListAppearanceController mTracksAppearanceController;

    public TracksPresenter(ITracksView view,
                           IRestClient restClient,
                           Context context,
                           TracksListAppearanceController tracksAppearanceController) {
        mView = view;
        mRestClient = restClient;
        mContext = context;
        mTracksAppearanceController = tracksAppearanceController;
    }

    @Override
    public void setSearchQuery(String query) {
        if(query.length() <=4)
        {
            mTracksAppearanceController.cleanAdapters();
            mView.showNotification(mContext.getString(R.string.give_five));
            return;
        }

        mTracksAppearanceController.cleanAdapters();
        mView.isProgressVivible(true);
        mRestClient.getTracksByName(query, this::onTracksReceived, this::onError);
    }

    @Override
    public void switchTracksView() {
        mTracksAppearanceController.switchAppearance();
        mView.setChangeViewIcon(mTracksAppearanceController.getMenuIcon());
        mView.changeTracksAppearance();
    }

    @Override
    public void onTrackClicked(List<Track> tracks, int index) {
        mView.openPlayer(tracks, index);
    }

    private void onTracksReceived(List<Track> tracks) {
        mTracks = tracks;
        mView.isProgressVivible(false);
        mTracksAppearanceController.updateAdapters(tracks);
        if(tracks.isEmpty())
            mView.showNotification(mContext.getString(R.string.nothing_found));
    }

    private void onError(Throwable throwable) {
        mView.isProgressVivible(false);
        mView.showNotification(throwable.getMessage());
    }

}
