package com.example.stephan.intechtest.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.stephan.intechtest.infrastructure.ImageLoaderConfigProvider;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.views.interfaces.ITrackViewHolderXmlProvider;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.List;

import rx.functions.Action2;

/**
 * Created by Stephan on 22.07.2016.
 */
public abstract class TracksBaseAdapter extends RecyclerView.Adapter<TableTrackViewHolder> {

    protected List<Track> mTracks;
    protected DisplayImageOptions mCoverOptions;
    protected Action2<List<Track>, Integer> mOnTrackClicked;

    public TracksBaseAdapter(List<Track> tracks,
                             Action2<List<Track>,
                              Integer> onTrackClickedHandler
                             )
    {
        mTracks = tracks;
        mCoverOptions = ImageLoaderConfigProvider.getOptions();
        mOnTrackClicked = onTrackClickedHandler;
    }

    protected void onTrackClicked(Integer index) {
        mOnTrackClicked.call(mTracks, index);
    }

    @Override
    public void onBindViewHolder(TableTrackViewHolder holder, int position) {
        Track track = mTracks.get(position);
        holder.showTrackInfo(track, mCoverOptions, position);
    }

    @Override
    public int getItemCount() {
        return mTracks.size();
    }

    public void updateData(List<Track> tracks)
    {
        mTracks = tracks;
        notifyDataSetChanged();
    }

    public void clean()
    {
        mTracks.clear();
        notifyDataSetChanged();
    }

}
