package com.example.stephan.intechtest.views.interfaces;

/**
 * Created by Stephan on 23.07.2016.
 */
public interface ISwitchViewIcontProvider {
    int getMenuIcon();
}
