package com.example.stephan.intechtest.views.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.stephan.intechtest.R;
import com.example.stephan.intechtest.model.Track;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Created by Stephan on 22.07.2016.
 */
public class TableTrackViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView mCover;
    private TextView mTrackName;
    private TextView mArtistName;
    private Action1<Integer> mOnClickHandler;
    private int mCurrentIndex;
    public View container;

    public TableTrackViewHolder(View itemView, Action1<Integer> onClick) {
        super(itemView);
        itemView.setOnClickListener(this);
        mCover = (ImageView)itemView.findViewById(R.id.track_cover);
        mTrackName = (TextView)itemView.findViewById(R.id.track_name);
        mArtistName = (TextView)itemView.findViewById(R.id.artist_name);
        mOnClickHandler = onClick;
        container = itemView.findViewById(R.id.container);
    }

    public void showTrackInfo(Track track, DisplayImageOptions options, int index)
    {
        mCurrentIndex = index;
        showTrackName(track.getTrackName());
        showArtistName(track.getArtistName());
        showCover(track.getCoverUrl(), options);
    }

    private void showTrackName(String name)
    {
        mTrackName.setText(name);
    }

    private void showArtistName(String name)
    {
        mArtistName.setText(name);
    }

    private void showCover(String url, DisplayImageOptions options)
    {
        ImageLoader.getInstance().displayImage(url, mCover, options);
    }

    @Override
    public void onClick(View v) {
        mOnClickHandler.call(mCurrentIndex);
    }
}
