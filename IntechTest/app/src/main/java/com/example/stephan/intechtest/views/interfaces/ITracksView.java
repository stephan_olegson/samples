package com.example.stephan.intechtest.views.interfaces;

import com.example.stephan.intechtest.model.Track;
import java.util.List;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface ITracksView {
    void showNotification(String text);
    void isProgressVivible(boolean isVisible);
    void setChangeViewIcon(int iconId);
    void changeTracksAppearance();
    void openPlayer(List<Track> tracks, int currentIndex);
}
