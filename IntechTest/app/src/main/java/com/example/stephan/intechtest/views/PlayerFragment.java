package com.example.stephan.intechtest.views;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stephan.intechtest.R;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.presenters.PlayerPresenter;
import com.example.stephan.intechtest.presenters.interfaces.IPlayerPresenter;
import com.example.stephan.intechtest.views.interfaces.IPlayerView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephan on 22.07.2016.
 */
public class PlayerFragment extends Fragment implements IPlayerView {

    private IPlayerPresenter mPresenter;
    private List<Track> mTracks;
    private int mShowTrackIndex;

    private ImageView mCoverView;
    private TextView mTrackNameView;
    private TextView mArtistNameView;

    private ImageButton mPlayButton;

    public static PlayerFragment newInstance()
    {
        PlayerFragment ret = new PlayerFragment();
        ret.setRetainInstance(true);
        return ret;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mTracks = new ArrayList<>();
        mShowTrackIndex = 0;
        mPresenter = new PlayerPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.player_fragment, container, false);

        Bundle args = getArguments();
        if(args!=null)
        {
            mTracks = args.getParcelableArrayList("TRACKS");
            mShowTrackIndex = args.getInt("TRACK_INDEX", 0);
            mPresenter.setData(mTracks, mShowTrackIndex);
        }

        mCoverView = (ImageView)view.findViewById(R.id.album_cover);
        mTrackNameView = (TextView)view.findViewById(R.id.track_name_in_player);
        mArtistNameView = (TextView)view.findViewById(R.id.artist_name_in_player);

        ImageButton nextTrack = (ImageButton)view.findViewById(R.id.next_track);
        nextTrack.setOnClickListener(v -> mPresenter.nextTrackPressed());

        ImageButton prevTrack = (ImageButton)view.findViewById(R.id.prev_track);
        prevTrack.setOnClickListener(v -> mPresenter.prevTrackPressed());

        mPlayButton = (ImageButton)view.findViewById(R.id.play_track);
        mPlayButton.setOnClickListener(v -> mPresenter.playTrackPressed());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    public void showCover(String url, DisplayImageOptions options) {
        ImageLoader.getInstance().displayImage(url, mCoverView, options);
    }

    @Override
    public void showTrackName(String name) {
        mTrackNameView.setText(name);
    }

    @Override
    public void showArtistName(String name) {
        mArtistNameView.setText(name);
    }

    @Override
    public void changePlayIcon(int id) {
        mPlayButton.setBackgroundResource(id);
    }
}
