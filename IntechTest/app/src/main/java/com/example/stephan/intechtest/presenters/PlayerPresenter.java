package com.example.stephan.intechtest.presenters;

import com.example.stephan.intechtest.R;
import com.example.stephan.intechtest.infrastructure.ImageLoaderConfigProvider;
import com.example.stephan.intechtest.model.Track;
import com.example.stephan.intechtest.presenters.interfaces.IPlayerPresenter;
import com.example.stephan.intechtest.views.interfaces.IPlayerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephan on 22.07.2016.
 */
public class PlayerPresenter implements IPlayerPresenter {

    private final IPlayerView mView;
    private  List<Track> mTracks;
    private int mShowIndex;
    private boolean isPlaying;

    public PlayerPresenter(IPlayerView view) {
        mView = view;
        mTracks = new ArrayList<>();
        mShowIndex = -1;
        isPlaying = false;
    }

    @Override
    public void onResume() {
        showTrack(mShowIndex);
    }

    @Override
    public void nextTrackPressed() {
        if(++mShowIndex>=mTracks.size()-1)
            mShowIndex=0;
        showTrack(mShowIndex);
    }

    @Override
    public void prevTrackPressed() {
        if(--mShowIndex < 0)
            mShowIndex=mTracks.size()-1;
        showTrack(mShowIndex);
    }

    @Override
    public void playTrackPressed() {
        isPlaying = isPlaying ? false : true;
        mView.changePlayIcon(isPlaying ? R.drawable.pause_button : R.drawable.play_button);
    }

    @Override
    public void setData(List<Track> tracks, int showTrackIndex) {
        mTracks = tracks;
        if(mShowIndex==-1)
            mShowIndex = showTrackIndex;
    }


    private void showTrack(int showIndex) {
        if(showIndex == -1)
            return;
        mView.showArtistName(mTracks.get(showIndex).getArtistName());
        mView.showTrackName(mTracks.get(showIndex).getTrackName());
        String bigCover = mTracks.get(showIndex).getCoverUrl().replace("100x100", "600x600");
        mView.showCover(bigCover, ImageLoaderConfigProvider.getOptions());
    }

}
