package com.example.stephan.intechtest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephan on 21.07.2016.
 */
public class TrackList {

    @SerializedName("resultCount")
    @Expose
    private Integer mTrackCount;

    @SerializedName("results")
    @Expose
    private List<Track> mTracks = new ArrayList<Track>();

    public Integer trackCount() {
        return mTrackCount;
    }

    public List<Track> getResults() {
        return mTracks;
    }
}
