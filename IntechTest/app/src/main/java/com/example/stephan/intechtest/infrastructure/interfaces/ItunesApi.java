package com.example.stephan.intechtest.infrastructure.interfaces;

import com.example.stephan.intechtest.model.TrackList;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface ItunesApi
{
    @GET("search")
    Observable<TrackList> getTracksByName(@Query("term") String query,
                                        @Query("entity") String type);

}