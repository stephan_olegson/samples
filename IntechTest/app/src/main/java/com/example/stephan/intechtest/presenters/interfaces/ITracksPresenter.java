package com.example.stephan.intechtest.presenters.interfaces;

import com.example.stephan.intechtest.model.Track;

import java.util.List;

/**
 * Created by Stephan on 21.07.2016.
 */
public interface ITracksPresenter {
    void setSearchQuery(String query);
    void switchTracksView();
    void onTrackClicked(List<Track> tracks, int index);
}
