package com.example.stephan.intechtest.presenters.interfaces;

import com.example.stephan.intechtest.model.Track;

import java.util.List;

/**
 * Created by Stephan on 24.07.2016.
 */
public interface IFragmentChanger {
    void changeToPlayer(List<Track> tracks, int currentIndex);
}
