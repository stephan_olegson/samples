package com.example.stephan.intechtest.views.interfaces;

import com.example.stephan.intechtest.views.adapters.TracksBaseAdapter;

/**
 * Created by Stephan on 23.07.2016.
 */
public interface ITracksAdaptersProvider {
    TracksBaseAdapter getAdapter();
}
