package home.com.snippets.list

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

/**
 * Created by Stephan on 14.06.2017.
 */
class HolderCreator<T: ViewDataBinding>(creator: (parent: ViewGroup?) -> Holder<T>) {
    private val holderCreator = creator
    fun create(parent: ViewGroup?) = holderCreator(parent)
}