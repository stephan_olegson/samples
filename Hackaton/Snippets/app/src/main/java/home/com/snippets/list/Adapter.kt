package home.com.snippets.list

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

/**
 * Created by Stephan on 14.06.2017.
 */
class Adapter<TBindignType: ViewDataBinding, TModelType>(holderCreator: HolderCreator<TBindignType>,
                    holderBinder: HolderBinder<TModelType,TBindignType>,
                    model: List<TModelType>) : RecyclerView.Adapter<Holder<TBindignType>>() {

    private val mHolderCreator = holderCreator
    private val mModel = model
    private val mBinder = holderBinder

    override fun onBindViewHolder(holder: Holder<TBindignType>, position: Int) {
        mBinder.bind(mModel[position], holder)
    }

    override fun getItemCount(): Int = mModel.count()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder<TBindignType> {
        return mHolderCreator.create(parent)
    }


}