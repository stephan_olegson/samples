package home.com.snippets.list

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

/**
 * Created by Stephan on 14.06.2017.
 */
class HolderBinder<TData, THolder : ViewDataBinding> (binder: (data: TData, holder: Holder<THolder>)-> Unit){
    private val mBinder = binder
    fun bind(data: TData, holder: Holder<THolder>) = mBinder(data, holder)
}