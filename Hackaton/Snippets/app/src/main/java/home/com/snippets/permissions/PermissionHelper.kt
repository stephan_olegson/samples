package home.com.snippets.permissions

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

/**
 * Created by rusulanov on 6/7/2017.
 */

object PermissionHelper
{
    private val ON_PERMISSION_RESULT: Int = 657
    enum class RequestResult{
        RationalNeeded, Usual
    }

    fun requestPermission(context: Activity, permission: String) : RequestResult
    {
        if(ActivityCompat.shouldShowRequestPermissionRationale(context, permission)){
            return RequestResult.RationalNeeded
        }
        else {
            showRequestDialog(context, permission)
            return RequestResult.Usual
        }
    }

    fun showRequestDialog(context: Activity, permission: String){
        ActivityCompat.requestPermissions(context, arrayOf(permission),
                ON_PERMISSION_RESULT)
    }

    fun isGranted(context: Activity, permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED)
            return true
        return false
    }

    //example
//    if(!PermissionHelper.isGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)){
//    when(PermissionHelper.requestPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)){
//        PermissionHelper.RequestResult.RationalNeeded -> {
//            val bar = Snackbar.make(findViewById(android.R.id.content),"I need it", Snackbar.LENGTH_INDEFINITE)
//            bar.setAction("got", {PermissionHelper.showRequestDialog(this, Manifest.permission.ACCESS_FINE_LOCATION)})
//            bar.show()
//        }
//        else -> {}
//    }
}