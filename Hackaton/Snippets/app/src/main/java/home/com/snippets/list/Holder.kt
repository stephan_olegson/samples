package home.com.snippets.list

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

/**
 * Created by rusulanov on 6/15/2017.
 */
class Holder<T: ViewDataBinding>(bind: T) : RecyclerView.ViewHolder(bind.root) {
    val binding = bind
}