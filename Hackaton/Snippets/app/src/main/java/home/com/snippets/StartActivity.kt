package home.com.snippets

import android.Manifest
import android.content.Context
import android.content.Intent
import android.database.DatabaseErrorHandler
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.ViewGroup
import android.widget.Toast
import home.com.snippets.camera.CameraHelper
import home.com.snippets.firebase.database.DatabseHelper
import home.com.snippets.list.Adapter
import home.com.snippets.permissions.PermissionHelper
import android.view.LayoutInflater
import android.widget.TextView
import home.com.snippets.databinding.ItemBinding
import home.com.snippets.list.Holder
import home.com.snippets.list.HolderBinder
import home.com.snippets.list.HolderCreator


class StartActivity : AppCompatActivity() {

//    var photoUri: Uri? = null
    var model: MutableList<String> = mutableListOf("one", "two", "three")
    var vm: SimpleVM? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val fab = findViewById(R.id.fab) as FloatingActionButton

//        val test = object {
//            val f1 = "Hello"
//            val f2 = "buy"
//        }
//
        fab.setOnClickListener {
             vm?.str = "PIZDFA"
        }
//            if (!PermissionHelper.isGranted(this, Manifest.permission.CAMERA)) {
//                when (PermissionHelper.requestPermission(this, Manifest.permission.CAMERA)) {
//                    PermissionHelper.RequestResult.RationalNeeded -> {
//                        val bar = Snackbar.make(findViewById(android.R.id.content), "I need it", Snackbar.LENGTH_INDEFINITE)
//                        bar.setAction("got", { PermissionHelper.showRequestDialog(this, Manifest.permission.CAMERA) })
//                        bar.show()
//                    }
//                    else -> {
//                    }
//                }
//                return@setOnClickListener
//            }
//            if (!PermissionHelper.isGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                when (PermissionHelper.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    PermissionHelper.RequestResult.RationalNeeded -> {
//                        val bar = Snackbar.make(findViewById(android.R.id.content), "I need it", Snackbar.LENGTH_INDEFINITE)
//                        bar.setAction("got", { PermissionHelper.showRequestDialog(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) })
//                        bar.show()
//                    }
//                    else -> {
//                    }
//                }
//                return@setOnClickListener
//            }
//            photoUri = CameraHelper.takePhoto(this@StartActivity, "testApp")
//        }

        val rec: RecyclerView = findViewById(R.id.list) as RecyclerView
        rec.layoutManager = LinearLayoutManager(this)

        val adap =  Adapter(HolderCreator(this::holderCreator), HolderBinder(this::holderBinder), model)
        rec.adapter = adap
    }

    fun holderCreator(parent: ViewGroup?): Holder<ItemBinding>{
        val inflater = LayoutInflater.from(parent!!.context)
        val bindign  = ItemBinding.inflate(inflater, parent, false)
        return Holder(bindign)
    }

    fun holderBinder(data: String, holder: Holder<ItemBinding>): Unit{
        if(vm == null){
            vm = SimpleVM(data)
            holder.binding.vm = vm
            return
        }
        holder.binding.vm = SimpleVM(data)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(CameraHelper.onTakeResult(requestCode, resultCode)){
            CameraHelper.Result.OK -> Toast.makeText(this, "Photo ok", Toast.LENGTH_SHORT).show()
            CameraHelper.Result.FAIL -> Toast.makeText(this, "Photo FAIL", Toast.LENGTH_SHORT).show()
            else -> {}
        }


    }
}
