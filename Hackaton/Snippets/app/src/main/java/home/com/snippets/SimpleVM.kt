package home.com.snippets

import android.databinding.BaseObservable
import android.databinding.Bindable
import home.com.snippets.BR
/**
 * Created by Stephan on 14.06.2017.
 */
class SimpleVM(s: String) : BaseObservable() {

    private var _str: String = s

    var str: String
    @Bindable
    get() = _str
    set(value){
        _str = value
        notifyPropertyChanged(BR.str)
    }
}