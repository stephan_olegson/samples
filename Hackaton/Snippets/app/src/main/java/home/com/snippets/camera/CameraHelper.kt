package home.com.snippets.camera

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object CameraHelper{
    private val TAKE_PHOTO_CODE: Int = 846

    enum class Result {
        OK, WRONG_CODE, FAIL
    }

    fun takePhoto(context: Activity, folder: String): Uri?
    {
        createFolder(folder)
        val m_ImageUri = createEmptyFile(folder, getFileName())

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, m_ImageUri)
        context.startActivityForResult(cameraIntent, TAKE_PHOTO_CODE)
        return m_ImageUri
    }

    fun onTakeResult(requestCode: Int, resultCode: Int): Result
    {
        if(requestCode != TAKE_PHOTO_CODE)
            return Result.WRONG_CODE

        if (resultCode == Activity.RESULT_OK)
            return Result.OK
        else
            return Result.FAIL
    }

    private fun createFolder(folderName: String) {
        val f = File(Environment.getExternalStorageDirectory(), folderName)
        if (!f.exists()) {
            f.mkdirs()
        }
    }

    private fun createEmptyFile(folder: String, fileName: String): Uri {
        val f = File(Environment.getExternalStorageDirectory().toString()
                + File.separator + folder + File.separator + fileName)
        return Uri.fromFile(f)
    }

    private fun getFileName(): String {
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss")
        val now = Date()
        val fileName = formatter.format(now) + ".png"
        return fileName
    }
}