package home.com.snippets

import android.app.Application
import com.google.firebase.FirebaseApp

/**
 * Created by rusulanov on 6/8/2017.
 */

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this);
    }
}