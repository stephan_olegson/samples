package home.com.snippets.firebase.database

import com.google.firebase.database.FirebaseDatabase

object DatabseHelper {
    private val mDatabse: FirebaseDatabase = FirebaseDatabase.getInstance()

    fun <T>putObject(path: String, value: T ){
        val ref = mDatabse.getReference(path)
        ref.setValue(value)
    }
}