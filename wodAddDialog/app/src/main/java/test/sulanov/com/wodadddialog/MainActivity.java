package test.sulanov.com.wodadddialog;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import test.sulanov.com.wodadddialog.databinding.ActivityMainBinding;
import test.sulanov.com.wodadddialog.views.AddWodActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setHandlers(this);

    }

    public void onAddWod(View view)
    {
        Intent intt = new Intent(this, AddWodActivity.class);
        startActivity(intt);

    }
}
