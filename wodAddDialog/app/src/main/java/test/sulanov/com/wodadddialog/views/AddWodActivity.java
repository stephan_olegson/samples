package test.sulanov.com.wodadddialog.views;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import test.sulanov.com.wodadddialog.R;

/**
 * Created by rusulanov on 9/20/2016.
 */
public class AddWodActivity extends FragmentActivity {

    private Fragment mAactivityContent;
    private final String TAG = "ACTIVITY_CONTENT";
    private AddWodPager mPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_wod_activity);
        mPager = new AddWodPager(getSupportFragmentManager());

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        vpPager.setAdapter(mPager);

//        FragmentManager fragmentManager = getSupportFragmentManager();
//        Fragment fragment = fragmentManager.findFragmentByTag(TAG);
//        if(fragment == null)
//        {
//            FragmentTransaction tran = fragmentManager.beginTransaction();
//
//            mAactivityContent = AddWodFragment.newInstance();
//            fragmentManager.beginTransaction()
//                    .add(R.id.content_holder, mAactivityContent, TAG)
//                    .commit();
//        }
//        else
//        {
//            mAactivityContent = fragment;
//        }

    }



    public class AddWodPager extends FragmentStatePagerAdapter {
        public AddWodPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = null;
            if(i==0)
                fragment = AddWodFragment.newInstance();
            else if(i==1)
                fragment = ExcsListFragment.newInstance();
            //TODO list fragment

//            Bundle args = new Bundle();
//            // Our object is just an integer :-P
//            args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1);
//            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

}


