package test.sulanov.com.wodadddialog.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import test.sulanov.com.wodadddialog.R;
import test.sulanov.com.wodadddialog.presenters.AddWodPresenter;
import test.sulanov.com.wodadddialog.presenters.IAddWodPresenter;

/**
 * Created by rusulanov on 9/20/2016.
 */
public class AddWodFragment extends Fragment implements IAddWodView{

    private IAddWodPresenter mPresenter;

    public static Fragment newInstance() {
        AddWodFragment frg = new AddWodFragment();
        frg.setRetainInstance(true);
        return frg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new AddWodPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.add_wod_fragment, container, false);



        return view;
    }
}
