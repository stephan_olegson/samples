package test.sulanov.com.wodadddialog.views;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import test.sulanov.com.wodadddialog.IWod;
import test.sulanov.com.wodadddialog.R;
import test.sulanov.com.wodadddialog.adapters.ExcWodAdapter;
import test.sulanov.com.wodadddialog.adapters.TouchHelper;

/**
 * Created by rusulanov on 9/20/2016.
 */
public class ExcsListFragment extends Fragment {

    private List<IWod> mWods;
    private ExcWodAdapter mAdapter;

    public static Fragment newInstance() {
        ExcsListFragment frg = new ExcsListFragment();
        frg.setRetainInstance(true);
        return frg;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Setup Adapter
        mAdapter= new ExcWodAdapter(getContext(), getWods());


        // Setup ItemTouchHelper


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.exs_list_fragment, container, false);


        RecyclerView movieRecyclerView = (RecyclerView) view.findViewById(R.id.exs_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        movieRecyclerView.setLayoutManager(linearLayoutManager);
        movieRecyclerView.setAdapter(mAdapter);

        ItemTouchHelper.Callback callback = new TouchHelper(mAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(movieRecyclerView);

        return view;
    }

    public List<IWod> getWods() {
        List<IWod> wods = new ArrayList<>();

        wods.add(new IWod("First"));
        wods.add(new IWod("Second"));
        wods.add(new IWod("Third"));
        wods.add(new IWod("Fourth"));

        return wods;
    }


}
