package test.sulanov.com.wodadddialog.adapters;

import android.content.Context;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import test.sulanov.com.wodadddialog.IWod;
import test.sulanov.com.wodadddialog.R;

/**
 * Created by rusulanov on 9/21/2016.
 */
public class ExcWodAdapter extends RecyclerView.Adapter<ExcWodAdapter.ViewHolder> {
    private Context mContext;
    private List<IWod> mWods;

    public ExcWodAdapter(Context context, List<IWod> wods){
        this.mContext = context;
        this.mWods = wods;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.wod_ex_item, parent, false));
        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindMovie(mWods.get(position));
    }

    @Override
    public int getItemCount() {
        return mWods.size();
    }

    public void remove(int position) {
        mWods.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition){
        Collections.swap(mWods, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public final TextView movieNameTextView;
        public View v;

        public ViewHolder(View view){
            super(view);
            movieNameTextView = (TextView) view.findViewById(R.id.wod_name);
            v = view.findViewById(R.id.touch_frame);
        }

        public void bindMovie(IWod movie){
            this.movieNameTextView.setText(movie.name);
        }
    }
}