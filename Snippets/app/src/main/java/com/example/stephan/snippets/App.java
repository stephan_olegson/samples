package com.example.stephan.snippets;

import android.app.Application;
import android.content.Intent;

/**
 * Created by Stephan on 25.07.2016.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
		
        startService(new Intent(getApplicationContext(), Service.class));
    }
}
