package com.example.stephan.snippets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

/**
 * Created by Stephan on 25.07.2016.
 */
public class Service extends android.app.Service {

    ServiceBinder binder = new ServiceBinder();

    public class ServiceBinder extends Binder
    {
        public Service getService ()
        {
            return Service.this;
        }
    }

    private class h extends Handler
    {
        public h(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sendMsg();
        }
    }
    private h H;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        HandlerThread thread = new HandlerThread("Thread name", android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        Looper serviceLooper = thread.getLooper();
        H = new h(serviceLooper);


        return START_NOT_STICKY;
    }

    public void requestMessage()
    {
        H.sendEmptyMessage(1);
    }

    public void sendMsg()
    {
        Intent inte = new Intent("test");
        inte.putExtra("data", "AZAZA");
        LocalBroadcastManager.getInstance(this).sendBroadcast(inte);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }


}
